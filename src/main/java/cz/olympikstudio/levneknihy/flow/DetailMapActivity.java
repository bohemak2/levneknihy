package cz.olympikstudio.levneknihy.flow;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.gms.maps.MapView;

import cz.olympikstudio.levneknihy.R;


abstract public class DetailMapActivity<T> extends DetailActivity<T> {
    protected MapView contentMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        contentMap = findViewById(R.id.content_map);
        contentMap.onCreate(new Bundle());

        setContentMapValue();

        setTitle(getTitleToolbar());
    }

    abstract protected void setContentMapValue();

    @Override
    public void onResume() {
        contentMap.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        contentMap.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        contentMap.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        contentMap.onLowMemory();
    }
}
