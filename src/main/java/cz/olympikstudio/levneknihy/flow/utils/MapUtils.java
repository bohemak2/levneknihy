package cz.olympikstudio.levneknihy.flow.utils;

import android.location.Location;

import java.util.Comparator;

public class MapUtils {

    public static double distance(Location location1, Location location2) {
        double theta = location1.getLongitude() - location2.getLongitude();
        double dist = Math.sin(deg2rad(location1.getLatitude()))
                * Math.sin(deg2rad(location2.getLatitude()))
                + Math.cos(deg2rad(location1.getLatitude()))
                * Math.cos(deg2rad(location2.getLatitude()))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public static Location createLocation(final float latitude, final float longitude){
        return new Location("AVAILABLE"){
            {
                setLatitude(latitude);
                setLongitude(longitude);
            }
        };
    }

    public static class EntityDistanced<T>{

        public EntityDistanced(T entity, float distance) {
            this.entity = entity;
            this.distance = distance;
        }

        public T entity;
        public float distance;
    }

    public static class SortByLocation implements Comparator<EntityDistanced>{

        @Override
        public int compare(EntityDistanced e1, EntityDistanced e2) {
            return (int)(e1.distance - e2.distance);
        }
    }
}
