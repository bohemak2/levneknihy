package cz.olympikstudio.levneknihy.flow;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import cz.olympikstudio.levneknihy.R;
import cz.olympikstudio.levneknihy.source.ESource;


abstract public class DetailActivity<T> extends BasicActivity {
    public static final String EXTRA_SOURCE = "detail_source";
    protected ESource source;

    protected ImageView contentImage;
    protected TextView contentText;
    protected T entity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        contentImage = findViewById(R.id.content_image);
        contentText = findViewById(R.id.content_text);

        entity = getEntity();

        setContentTextValue();
        setContentImageValue();

        setTitle(getTitleToolbar());

        if (getIntent().getSerializableExtra(EXTRA_SOURCE) != null){
            source = ESource.valueOf((String)getIntent().getSerializableExtra(EXTRA_SOURCE));
        }
    }


    abstract protected void setContentImageValue();

    abstract protected void setContentTextValue();

    abstract protected String getTitleToolbar();

    abstract protected T getEntity();

    @Override
    public void setLayout() {
        setContentView(R.layout.content_item_detail);
    }

    public static int getImageResourcePreview(){
        return R.drawable.ic_broken_image;
    }
}
