package cz.olympikstudio.levneknihy.flow;

import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;

import cz.olympikstudio.levneknihy.R;
import cz.olympikstudio.levneknihy.ui.buttons.EFloatableButton;
import cz.olympikstudio.levneknihy.ui.buttons.IFloatable;
import cz.olympikstudio.levneknihy.ui.messages.EMessage;
import cz.olympikstudio.levneknihy.ui.messages.IMessagable;

abstract public class BasicActivity extends AppCompatActivity implements IFloatable<EFloatableButton>, IMessagable<EMessage> {

    private HashMap<EFloatableButton, FloatingActionButton> floatableButtons = new HashMap<>();
    protected Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout();

        /* Toolbar */
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        /* FloatingButtons */
        for (EFloatableButton type :  EFloatableButton.values()){
            FloatingActionButton _b = null;

            switch (type){
                case LEFT_BIG:
                    _b = findViewById(R.id.fab_left_big);
                    break;
                case LEFT_SMALL:
                    _b = findViewById(R.id.fab_left_small);
                    break;
                case RIGHT_BIG:
                    _b = findViewById(R.id.fab_right_big);
                    break;
                case RIGHT_SMALL:
                    _b = findViewById(R.id.fab_right_small);
                    break;
            }

            /* Each activity must turn on by 'setFloatableButton' */
            if (_b != null){
                _b.hide();

                floatableButtons.put(type, _b);
            }
        }
    }

    abstract public void setLayout();

    @Override
    public FloatingActionButton getFloatableButton(EFloatableButton type){
        return floatableButtons.get(type);
    }

    @Override
    public void setFloatableButton(EFloatableButton type, int colorRes, int imageRes, View.OnClickListener listener) {
        FloatingActionButton b_ = getFloatableButton(type);

        if (b_ != null){
            b_.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(colorRes)));
            b_.setImageResource(imageRes);
            b_.setOnClickListener(listener);

            b_.show();
        }
    }

    @Override
    public void showMessage(EMessage type, int textRes) {
        showMessage(type, getString(textRes));
    }

    @Override
    public void showMessage(EMessage type, String text) {
        switch (type){
            case DIALOG:
                Dialog dialog = new Dialog(BasicActivity.this);
                dialog.setContentView(R.layout.custom_dialog);

                TextView textView = dialog.findViewById(R.id.dialog_text);
                textView.setText(text);
                textView.setMovementMethod(new ScrollingMovementMethod());

                dialog.show();
                break;
            case SNACKBAR:
                Snackbar.make(toolbar, text, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                break;
        }
    }

    public void setTitle(int resStr) {
        toolbar.setTitle(resStr);
    }

    protected boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo =
                ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))
                        .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
