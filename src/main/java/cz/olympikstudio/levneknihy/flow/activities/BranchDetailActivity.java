package cz.olympikstudio.levneknihy.flow.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.RequiresApi;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import cz.olympikstudio.levneknihy.R;
import cz.olympikstudio.levneknihy.data.Database;
import cz.olympikstudio.levneknihy.data.bo.branch.Branch;
import cz.olympikstudio.levneknihy.flow.DetailMapActivity;
import cz.olympikstudio.levneknihy.flow.utils.MapUtils;
import cz.olympikstudio.levneknihy.ui.buttons.EFloatableButton;
import cz.olympikstudio.levneknihy.ui.messages.EMessage;

public class BranchDetailActivity extends DetailMapActivity<Branch> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setFloatableButton(EFloatableButton.LEFT_BIG
                , R.color.colorPrimaryDark
                , R.drawable.ic_map
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("geo:0,0?q=" + entity.getLat() + "," + entity.getLng() + "(" + entity.getName() + ")"));
                            startActivity((Intent.createChooser(intent, "Ukázat: " + entity.getName())));
                        }
                    }
                }
        );

        setFloatableButton(EFloatableButton.LEFT_SMALL
                , R.color.colorPrimaryDark
                , R.drawable.ic_menu_share
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        {
                            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "\n\n");
                            intent.putExtra(android.content.Intent.EXTRA_TEXT, entity.getUrl());
                            startActivity(Intent.createChooser(intent, "Sdílet: " + entity.getName()));
                        }
                    }
                }
        );

        setFloatableButton(EFloatableButton.RIGHT_BIG
                , R.color.colorDownload
                , R.drawable.ic_distance
                , new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onClick(View view) {
                        Location location = null;

                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                            location = manager.getLastKnownLocation(manager.getBestProvider(new Criteria(), false));
                        }

                        if (location != null) {
                            showMessage(EMessage.SNACKBAR, entity.getName() + " je " + (location.distanceTo(MapUtils.createLocation(entity.getLat(), entity.getLng())) / 1000) + " km daleko.");
                        } else {
                            showMessage(EMessage.SNACKBAR, R.string.navigation_no_gps);
                        }
                    }
            }
        );
    }

    @Override
    protected void setContentImageValue() {
        if(contentImage != null){
            contentImage.setVisibility(View.GONE);

        }
    }

    @Override
    protected void setContentMapValue() {
        if (contentMap != null){
            contentMap.setVisibility(View.VISIBLE);
            contentMap.getMapAsync(new OnMapReadyCallback() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    LatLng position = new LatLng(entity.getLat(), entity.getLng());


                    googleMap.addMarker(new MarkerOptions().position(position));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 16f));

                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MainActivity.REQUEST_PERMISSION_ACCESS_FINE_LOCATION);
                    }

                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        googleMap.setMyLocationEnabled(true);
                    }
                }
            });
        }
    }

    @Override
    protected void setContentTextValue() {
        if(contentText != null){
            contentText.setVisibility(View.VISIBLE);
            contentText.setText(entity.getLat() + ", " + entity.getLng() + "\n\n" + entity.getAddress() + "\n\n" + entity.getHours());
        }
    }

    @Override
    protected Branch getEntity() {
        return Database.getDB(this).getBranchDao().getOne(getIntent().getData().toString());
    }

    @Override
    protected String getTitleToolbar() {
        return entity.getName();
    }
}
