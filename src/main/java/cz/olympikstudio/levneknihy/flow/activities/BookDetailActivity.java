package cz.olympikstudio.levneknihy.flow.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import cz.olympikstudio.levneknihy.R;
import cz.olympikstudio.levneknihy.data.Database;
import cz.olympikstudio.levneknihy.data.bo.book.Book;
import cz.olympikstudio.levneknihy.data.bo.branch.Branch;
import cz.olympikstudio.levneknihy.flow.DetailActivity;
import cz.olympikstudio.levneknihy.source.LevneKnihy;
import cz.olympikstudio.levneknihy.source.LevneKnihyEModel;
import cz.olympikstudio.levneknihy.source.downloader.Downloader;
import cz.olympikstudio.levneknihy.source.downloader.EDownloadType;
import cz.olympikstudio.levneknihy.ui.buttons.EFloatableButton;
import cz.olympikstudio.levneknihy.ui.messages.EMessage;

public class BookDetailActivity extends DetailActivity<Book> {

    public static final String EXTRA_BOOK_SAVED = "detail_book_saved";
    private Intent intent;

    private boolean wasSaved;
    private boolean isSaved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        intent = new Intent(){
            {
                putExtra(MainActivity.RESPONSE_DETAIL_URL_STRING, entity.getUrl());
            }
        };

        String savedBookId = getIntent().getExtras().getString(EXTRA_BOOK_SAVED);
        if (savedBookId != null){

            if (Database.getDB(this).getBookSavedDao().getOne(savedBookId) != null){
                wasSaved = true;
                isSaved = true;
                setContentTextValue();
            }
        }


        setFloatableButton(EFloatableButton.LEFT_BIG
                , R.color.colorAccent
                , wasSaved ? R.drawable.ic_star_filled : R.drawable.ic_star_border
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (source) {
                            case LEVNE_KNIHY:
                                if (isSaved){
                                    showMessage(EMessage.SNACKBAR, R.string.removed);
                                } else {
                                    showMessage(EMessage.SNACKBAR, R.string.saved);
                                }
                                isSaved = !isSaved;

                                setContentTextValue();
                                getFloatableButton(EFloatableButton.LEFT_BIG).setImageResource(isSaved ? R.drawable.ic_star_filled : R.drawable.ic_star_border);
                                break;
                            default:
                                showMessage(EMessage.SNACKBAR, R.string.synchronization_error);
                        }
                    }
                }
        );

        setFloatableButton(EFloatableButton.LEFT_SMALL
                , R.color.colorPrimaryDark
                , R.drawable.ic_menu_share
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        {
                            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "\n\n");
                            intent.putExtra(android.content.Intent.EXTRA_TEXT, entity.getUrl());
                            startActivity(Intent.createChooser(intent, "Sdílet: " + entity.getName()));
                        }
                    }
                }
        );


        setFloatableButton(EFloatableButton.RIGHT_BIG
                , R.color.colorDownload
                , R.drawable.ic_store
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (source != null){
                            switch (source) {
                                case LEVNE_KNIHY:
                                    try {

                                        LevneKnihy dataSource = new LevneKnihy(BookDetailActivity.this, Database.getDB(BookDetailActivity.this));

                                        List<String> branchesId = new ArrayList<>();
                                        List<Branch> branches = (List<Branch>) dataSource.get(LevneKnihyEModel.BRANCH);
                                        String print = "";

                                        if(isNetworkAvailable()){
                                            String url = dataSource.getBookAvailabilityUrl(entity);
                                            if (url != null){
                                                JSONObject data = new Downloader<JSONObject>(EDownloadType.JSON).execute(new URL(url)).get();

                                                if (data != null) {
                                                    JSONArray branchesJson = data.getJSONArray("BranchOfficeDeliveryDatesList");

                                                    for (int i = 0; i < branchesJson.length(); i++) {
                                                        String branchName = branchesJson.getJSONObject(i).getString("BranchOfficeName");
                                                        String branchUrlize = Normalizer.normalize(branchName, Normalizer.Form.NFD).toLowerCase().replace("   ", "").replace("  ", "").replace("-", "").replace("  ", " ").replace(" ", "-").replace(".", "").replace("(", "").replace(")", "").replaceAll("\\p{InCombiningDiacriticalMarks}+", "");

                                                        int branchAvailability = branchesJson.getJSONObject(i).getInt("AvailableCount");

                                                        // Flag for no
                                                        boolean flag = true;

                                                        for (Branch branch : branches) {
                                                            // TODO: BranchDetailPage
                                                            // Jedina moznost, jak sparovat branch z JSONu a DB (bohuzel ne bezchybna)
                                                            if (branchAvailability > 0 && branch.getUrl().contains(branchUrlize)) {
                                                                branchesId.add(branch.getId());
                                                                print += "\n" + branchName + ": " + branchAvailability + " ks";
                                                                flag = false;
                                                                break;
                                                            }
                                                        }

                                                        if(flag && branchAvailability > 0){
                                                            print += "\n" + branchName + ": " + branchAvailability + " ks";
                                                        }
                                                    }

                                                    entity.setBranches(branchesId);
                                                    setContentTextValue();
                                                    Database.getDB(BookDetailActivity.this).getBookDao().update(new ArrayList() {{add(entity);}});

                                                    if (branchesId.size() > 0){
                                                        print = entity.getName() + " se nachází v " + branchesId.size() + " pobočkách:" + print;
                                                    } else {
                                                        print = entity.getName() + " se nachází v pobočkách:" + print;
                                                    }
                                                }
                                            }
                                        } else {
                                            print = "Jste offline. Po poslední synchronizaci: ";

                                            for(Branch branch : entity.getBranchesBO(branches)){
                                                String branchName = branch.getName();
                                                if (branchName == null){
                                                    branchName = branch.getUrl();
                                                }
                                                print += "\n" + branchName;
                                            }
                                        }

                                        if (print.length() == 0){
                                            print = "Nenalezeno v žádné pobočce. Buď je kniha nedostupná, nebo nemáte načtený seznam poboček.";
                                        }

                                        showMessage(EMessage.DIALOG, print);
                                    } catch (Exception e){
                                        e.printStackTrace();
                                        showMessage(EMessage.SNACKBAR, R.string.synchronization_error);
                                    }

                                break;
                            default:
                                showMessage(EMessage.SNACKBAR, R.string.synchronization_error);
                            }
                        } else {
                            showMessage(EMessage.SNACKBAR, R.string.synchronization_error);
                        }
                    }
                }
        );

        setFloatableButton(EFloatableButton.RIGHT_SMALL
                , R.color.colorDownload
                , R.drawable.ic_image
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(isNetworkAvailable()){
                            String url = entity.getImageUrl();
                            byte[] result = null;

                            if (url != null){
                                try {
                                    result = new Downloader<byte[]>(EDownloadType.BYTES).execute(new URL(entity.getImageUrl())).get();
                                } catch (Exception e){
                                    e.printStackTrace();
                                }
                            }

                            if (result != null){
                                showMessage(EMessage.SNACKBAR, R.string.loaded);
                                entity.setImage(result);
                                setContentImageValue();
                                Database.getDB(BookDetailActivity.this).getBookDao().update(new ArrayList(){ { add(entity); } });
                            } else {
                                showMessage(EMessage.SNACKBAR, R.string.loaded_no);
                            }
                        } else {
                            showMessage(EMessage.SNACKBAR, R.string.connection_error);
                        }
                    }
                }
        );
    }

    @Override
    protected void setContentImageValue(){
        byte[] image = entity.getImage();

        if (image != null && image.length > 0 && contentImage != null){
            contentImage.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(image, 0, image.length), contentImage.getLayoutParams().width, contentImage.getLayoutParams().height, false));
        } else {
            contentImage.setImageResource(this.getImageResourcePreview());
        }
    }

    @Override
    protected void setContentTextValue(){
        if (contentText != null){
            contentText.setVisibility(View.VISIBLE);

            String text = "";

            if (entity.getAuthor().length() > 0){
                text += entity.getAuthor() + "\t|\t";
            }

            if(entity.getBranches() != null && entity.getBranches().size() > 0){
                text += "K dispozici v " + entity.getBranches().size() + " pobočkách\t|\t";
            }

            text += entity.getPrice() + " ,- Kč";

            if(isSaved){
                text += "\nUloženo v oblíbených";
            }

            text += "\n\n" + entity.getDescription();

            contentText.setText(text);
        }
    }

    @Override
    protected Book getEntity() {
        return Database.getDB(this).getBookDao().getOne(getIntent().getData().toString());
    }

    @Override
    protected String getTitleToolbar() {
        return entity.getName();
    }

    @Override
    public void onBackPressed() {
        if (wasSaved != isSaved){
            if (isSaved){
                intent.putExtra(MainActivity.RESPONSE_DETAIL_SAVED_INSERT_BOOLEAN, true);
            } else {
                intent.putExtra(MainActivity.RESPONSE_DETAIL_SAVED_DELETE_BOOLEAN, true);
            }
        }
        setResult(MainActivity.RESPONSE_DETAIL_CODE, intent);
        finishActivity(MainActivity.REQUEST_DETAIL);
        super.onBackPressed();
    }
}
