package cz.olympikstudio.levneknihy.flow.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.olympikstudio.levneknihy.R;
import cz.olympikstudio.levneknihy.data.Database;
import cz.olympikstudio.levneknihy.data.bo.book.Book;
import cz.olympikstudio.levneknihy.data.bo.book.BookSaved;
import cz.olympikstudio.levneknihy.data.bo.branch.Branch;
import cz.olympikstudio.levneknihy.flow.BasicActivity;
import cz.olympikstudio.levneknihy.flow.utils.MapUtils;
import cz.olympikstudio.levneknihy.source.ESource;
import cz.olympikstudio.levneknihy.source.LevneKnihy;
import cz.olympikstudio.levneknihy.source.LevneKnihyEModel;
import cz.olympikstudio.levneknihy.ui.adapter.BookAdapter;
import cz.olympikstudio.levneknihy.ui.adapter.BranchAdapter;
import cz.olympikstudio.levneknihy.ui.adapter.ModelableAdapter;
import cz.olympikstudio.levneknihy.ui.buttons.EFloatableButton;
import cz.olympikstudio.levneknihy.ui.messages.EMessage;

public class MainActivity extends BasicActivity implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    public static final int REQUEST_DETAIL = 1;
    public static final int RESPONSE_DETAIL_CODE = 1;
    public static final String RESPONSE_DETAIL_URL_STRING = "detail_id";
    public static final String RESPONSE_DETAIL_SAVED_DELETE_BOOLEAN = "detal_delete";
    public static final String RESPONSE_DETAIL_SAVED_INSERT_BOOLEAN = "detal_insert";

    public static final int REQUEST_PERMISSION_ACCESS_FINE_LOCATION = 1;

    ConstraintLayout contentLayout;
    LevneKnihy lk;
    NavigationView navigationView;
    TextView contentText;
    RecyclerView recyclerView;
    MenuItem searchItem;
    MenuItem verticalItem;
    SearchView searchView;

    // Welcome in app.
    // We use SQLite database trough Android Room - singleton created if getDB called.
    // We use direct references in Source children (e.g. LK) as unique source of true (aka Redux/Elm).
    // So any BL and persistence occurre in Source. Source holds multiple models (as Enum aka Books/Branches/Reviews... Whatever).

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lk = new LevneKnihy(this, Database.getDB(this));

        contentLayout = findViewById(R.id.content_layout);
        navigationView = findViewById(R.id.nav_view);

        contentText = findViewById(R.id.content_text);
        contentText.setText(R.string.navigation_start);


//      Je to mozne?  recyclerView = new RecyclerView(getApplicationContext());
        recyclerView = findViewById(R.id.recycler_view);

        final Context context = this.getApplicationContext();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

//        contentLayout.addView(recyclerView);

        setFloatableButton(EFloatableButton.LEFT_BIG
            , R.color.colorWarning
            , R.drawable.ic_delete
            , new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LevneKnihyEModel type = null;

                    switch (navigationView.getCheckedItem().getItemId()){
                        case (R.id.nav_books):
                            type = LevneKnihyEModel.BOOK;
                            break;
                        case (R.id.nav_branches):
                            type = LevneKnihyEModel.BRANCH;
                            break;
                    }

                    if (type != null){
                        lk.erase(type, lk.get(type));
                        recyclerView.getAdapter().notifyDataSetChanged();

                        showMessage(EMessage.SNACKBAR, R.string.removed);
                    }

                }
            }
        );

        setFloatableButton(EFloatableButton.LEFT_SMALL
            , R.color.colorDownload
            , R.drawable.ic_url_download
            , new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isNetworkAvailable()){
                        showMessage(EMessage.SNACKBAR, R.string.connection_error);
                    } else {
                        switch (navigationView.getCheckedItem().getItemId()){
                            case (R.id.nav_books):
                                lk.downloadList(LevneKnihyEModel.BOOK);
                                break;
                            case (R.id.nav_branches):
                                lk.downloadList(LevneKnihyEModel.BRANCH);
                                break;
                        }

                        recyclerView.getAdapter().notifyDataSetChanged();
                        showMessage(EMessage.SNACKBAR, R.string.synchronization);
                    }
                }
            }
        );

        setFloatableButton(EFloatableButton.RIGHT_BIG
            , R.color.colorDownload
            , R.drawable.ic_download_all
            , new View.OnClickListener() {
                @Override
                public void onClick(final View view) {

                    if (!isNetworkAvailable()){
                        showMessage(EMessage.SNACKBAR, R.string.connection_error);
                    } else {
                        (new Thread(){
                            @Override
                            public void run() {
                                switch (navigationView.getCheckedItem().getItemId()){
                                    case (R.id.nav_branches):
                                        for (final Branch branch : (List<Branch>)lk.get(LevneKnihyEModel.BRANCH)) {
                                            lk.downloadDetail(LevneKnihyEModel.BRANCH, branch);
                                            MainActivity.this.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    recyclerView.getAdapter().notifyDataSetChanged();
                                                    showMessage(EMessage.SNACKBAR, branch.getName() + " načten/o/a..");
                                                }
                                            });
                                        }
                                        showMessage(EMessage.SNACKBAR, R.string.synchronization);
                                        break;
                                }
                            }
                        }).start();
                    }
                }
            }
        );

        // Hide on homepage
        for(EFloatableButton type : EFloatableButton.values()){
            getFloatableButton(type).hide();
        }
        try {
            findViewById(R.id.content_map).setVisibility(View.GONE);
        } catch(Exception e){
            e.printStackTrace();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void setLayout() {
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_DETAIL){

            if(resultCode == RESPONSE_DETAIL_CODE){
                String url = data.getStringExtra(RESPONSE_DETAIL_URL_STRING);

                if (url != null){
                    for(Book book : (List<Book>)lk.get(LevneKnihyEModel.BOOK)){
                        if (book.getUrl().equals(url)){
                            Gson gson = new Gson();
                            final BookSaved entity = gson.fromJson(gson.toJson(book), BookSaved.class);

                            if(data.getBooleanExtra(RESPONSE_DETAIL_SAVED_DELETE_BOOLEAN, false)){
                                lk.erase(LevneKnihyEModel.BOOK_SAVED, new ArrayList() {{add(entity); }});
                            }

                            if(data.getBooleanExtra(RESPONSE_DETAIL_SAVED_INSERT_BOOLEAN, false)){
                                lk.insert(LevneKnihyEModel.BOOK_SAVED, new ArrayList() {{add(entity); }});
                            }

                            recyclerView.getAdapter().notifyDataSetChanged();

                            break;
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        searchItem = menu.findItem(R.id.action_search);

        searchView = (SearchView)searchItem.getActionView();
        searchView.setOnQueryTextListener(MainActivity.this);
        searchView.getRootView().setVisibility(View.GONE);

        verticalItem = menu.findItem(R.id.action_vertical);
        verticalItem.setVisible(false);

        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_vertical) {
            Location location = null;

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_ACCESS_FINE_LOCATION);
            }

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                location = manager.getLastKnownLocation(manager.getBestProvider(new Criteria(), false));
            }

            if (location != null) {
                List<MapUtils.EntityDistanced<Branch>> distanced = new ArrayList<>();
                List<Branch> unloaded = new ArrayList<>();
                List<Branch> branches = (List<Branch>)lk.get(LevneKnihyEModel.BRANCH);

                for (final Branch branch : branches){
                    try {
                        distanced.add(new MapUtils.EntityDistanced<>(branch, location.distanceTo(MapUtils.createLocation(branch.getLat(), branch.getLng()))));
                    } catch (Exception e){
                        unloaded.add(branch);
                    }
                }
                // Handle on unique reference - clear all...
                branches.clear();
                // ...apend entities without longitude and latitude
                branches.addAll(unloaded);
                // ...sort entities with longitude and latitude
                Collections.sort(distanced, new MapUtils.SortByLocation());
                for (MapUtils.EntityDistanced<Branch> distancedE : distanced){
                    branches.add(distancedE.entity);
                }

                try {
                    MapUtils.EntityDistanced<Branch> nearest = distanced.get(0);
                    showMessage(EMessage.SNACKBAR, "Nejbližší pobočka " + nearest.entity.getName() + " je " + (nearest.distance / 1000) + " km daleko.");
                    recyclerView.getAdapter().notifyDataSetChanged();
                } catch (Exception e){
                    showMessage(EMessage.SNACKBAR, R.string.navigation_no_nearest);
                }
            } else {
                showMessage(EMessage.SNACKBAR, R.string.navigation_no_gps);
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        int id = item.getItemId();

        setAdapter(id);
        setContentText(id);
        setToolbarTitle(id);
        setVisibility(id);
        filter(id, searchView.getQuery().toString());
        return true;
    }


    private void setContentText(int id){

        switch (id){
            case R.id.nav_books:
            case R.id.nav_books_saved:
            case R.id.nav_branches:
                contentText.setText(R.string.content_download);
                break;

        }
    }

    private void setToolbarTitle(int id){

        switch (id){
            case R.id.nav_books:
                setTitle(R.string.menu_books);
                break;
            case R.id.nav_books_saved:
                setTitle(R.string.menu_books_saved);
                break;
            case R.id.nav_branches:
                setTitle(R.string.menu_branches);
                break;
        }
    }

    private void setAdapter(int id){
        ModelableAdapter adapter = null;

        switch (id){
            case R.id.nav_books:
            case R.id.nav_books_saved:
                LevneKnihyEModel type = null;

                if (id == R.id.nav_books){
                    type = LevneKnihyEModel.BOOK;
                } else if (id == R.id.nav_books_saved){
                    type = LevneKnihyEModel.BOOK_SAVED;
                }

                adapter = new BookAdapter((List<Book>)lk.get(type)) {

                    @Override
                    public void onItemClick(final Book book, final int i) {
                        if (book.getId() != null) {
                            MainActivity.this.startActivityForResult(new Intent(MainActivity.this, BookDetailActivity.class) {
                                {
                                    String id = book.getId();
                                    setData(Uri.parse(id));
                                    putExtra(BookDetailActivity.EXTRA_SOURCE, ESource.LEVNE_KNIHY.toString());

                                    for(BookSaved bookSaved : (List<BookSaved>)lk.get(LevneKnihyEModel.BOOK_SAVED)){
                                        if (id.equals(bookSaved.getId())){
                                            putExtra(BookDetailActivity.EXTRA_BOOK_SAVED, id);
                                            break;
                                        }
                                    }
                                }
                            }, REQUEST_DETAIL);
                        }
                    }

                    @Override
                    public void onDownloadClick(final Book book, final int i) {
                        if (!isNetworkAvailable()){
                            showMessage(EMessage.SNACKBAR, R.string.connection_error);
                        } else {
                            (new Thread() {

                                @Override
                                public void run() {
                                    lk.downloadDetail(LevneKnihyEModel.BOOK, book);

                                    // Fixed CalledFromWrongThreadException
                                    MainActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            recyclerView.getAdapter().notifyItemChanged(i);
                                            showMessage(EMessage.SNACKBAR, book.getName() + " načten/o/a..");
                                        }
                                    });
                                }
                            }).start();
                        }
                    }

                    {
                        registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                            @Override
                            public void onChanged() {
                                super.onChanged();

                                if (lk.get(LevneKnihyEModel.BOOK).isEmpty()) {
                                    contentText.setVisibility(View.VISIBLE);
                                } else {
                                    contentText.setVisibility(View.GONE);
                                }
                            }
                        });

                    }
                };
                break;
            case R.id.nav_branches:
                adapter = new BranchAdapter((List<Branch>)lk.get(LevneKnihyEModel.BRANCH)) {
                    @Override
                    public void onItemClick(final Branch branch, final int i) {
                        if (branch.getId() != null && branch.getAddress() != null){
                            MainActivity.this.startActivity(new Intent(MainActivity.this, BranchDetailActivity.class){
                                {
                                    setData(Uri.parse(branch.getId()));
                                }
                            });
                        }
                    }

                    @Override
                    public void onDownloadClick(final Branch branch, final int i) {
                        if (!isNetworkAvailable()){
                            showMessage(EMessage.SNACKBAR, R.string.connection_error);
                        } else {
                            (new Thread(){

                                @Override
                                public void run() {
                                    lk.downloadDetail(LevneKnihyEModel.BRANCH, branch);

                                    // Fixed CalledFromWrongThreadException
                                    MainActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            recyclerView.getAdapter().notifyItemChanged(i);
                                            showMessage(EMessage.SNACKBAR, branch.getName() + " načten/o/a..");
                                        }
                                    });
                                }
                            }).start();
                        }
                    }

                    {
                        registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                            @Override
                            public void onChanged() {
                                super.onChanged();

                                if(lk.get(LevneKnihyEModel.BRANCH).isEmpty()){
                                    contentText.setVisibility(View.VISIBLE);
                                } else {
                                    contentText.setVisibility(View.GONE);
                                }
                            }
                        });

                    }
                };
                break;
        }

        if (adapter != null){
            recyclerView.setAdapter(adapter);
        }
    }

    private void setVisibility(int id){

        switch (id){
            case R.id.nav_books:
                getFloatableButton(EFloatableButton.LEFT_BIG).show();
                getFloatableButton(EFloatableButton.LEFT_SMALL).show();
                getFloatableButton(EFloatableButton.RIGHT_BIG).hide();
                contentText.setVisibility(View.GONE);
                verticalItem.setVisible(false);
                break;

            case R.id.nav_books_saved:
                getFloatableButton(EFloatableButton.LEFT_BIG).show();
                getFloatableButton(EFloatableButton.LEFT_SMALL).hide();
                getFloatableButton(EFloatableButton.RIGHT_BIG).hide();
                contentText.setVisibility(View.GONE);
                verticalItem.setVisible(false);
                break;

            case R.id.nav_branches:
                getFloatableButton(EFloatableButton.LEFT_BIG).show();
                getFloatableButton(EFloatableButton.LEFT_SMALL).show();
                getFloatableButton(EFloatableButton.RIGHT_BIG).show();
                contentText.setVisibility(View.GONE);
                verticalItem.setVisible(true);
                verticalItem.setTitle(R.string.branch_order_nearest);
                break;
        }

        if (id == R.id.nav_books || id == R.id.nav_books_saved || id == R.id.nav_branches ){
            searchView.getRootView().setVisibility(View.VISIBLE);
        } else {
            searchView.getRootView().setVisibility(View.GONE);
        }
    }

    // TODO: ROZHODNOUT, CO JE SPRAVE. Taky moznost vyuzit u adapteru: mAdapter.replaceAll(List<>);
    @Override
    public boolean onQueryTextSubmit(String s) {
        filter(navigationView.getCheckedItem().getItemId(), s);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        filter(navigationView.getCheckedItem().getItemId(), s);
        return false;
    }

    private void filter(int id, String s){
        switch (id){
            case (R.id.nav_books):
                lk.fulltext(LevneKnihyEModel.BOOK, s);
                break;

            case (R.id.nav_books_saved):
                lk.fulltext(LevneKnihyEModel.BOOK_SAVED, s);
                break;

            case (R.id.nav_branches):
                lk.fulltext(LevneKnihyEModel.BRANCH, s);
                break;
        }
        if(s.length() > 0){
            searchItem.setIcon(R.drawable.ic_search_active);
        } else {
            searchItem.setIcon(R.drawable.ic_search);
        }

        recyclerView.getAdapter().notifyDataSetChanged();
    }
}
