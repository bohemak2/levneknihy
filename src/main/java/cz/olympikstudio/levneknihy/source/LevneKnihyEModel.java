package cz.olympikstudio.levneknihy.source;

import cz.olympikstudio.levneknihy.data.bo.IModelable;
import cz.olympikstudio.levneknihy.data.bo.book.Book;
import cz.olympikstudio.levneknihy.data.bo.book.BookSaved;
import cz.olympikstudio.levneknihy.data.bo.branch.Branch;

public enum LevneKnihyEModel implements IEModel {
    BOOK, BOOK_SAVED, BRANCH;

    public Class<? extends IModelable> classBO (){
        switch (this){
            case BOOK:
                return Book.class;
            case BOOK_SAVED:
                return BookSaved.class;
            case BRANCH:
                return Branch.class;
        }

        return null;
    }
}
