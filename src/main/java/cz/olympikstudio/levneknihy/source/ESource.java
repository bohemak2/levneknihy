package cz.olympikstudio.levneknihy.source;

public enum ESource {
    LEVNE_KNIHY;

    public IEModel[] getSourceModels(){
        switch (this){
            case LEVNE_KNIHY:
                return LevneKnihyEModel.values();
            default:
                return new IEModel[]{};
        }
    }
}
