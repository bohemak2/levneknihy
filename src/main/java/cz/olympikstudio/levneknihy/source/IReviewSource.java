package cz.olympikstudio.levneknihy.source;

import java.io.IOException;
import java.util.List;

import cz.olympikstudio.levneknihy.data.bo.Review;

public interface IReviewSource {
    List<Review> getReviews(boolean cache) throws IOException;
}
