package cz.olympikstudio.levneknihy.source;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.io.Serializable;

public abstract class Source implements ISource, Serializable {
    public static Gson gson;
    public static SharedPreferences preferences;

    public Source(Context context) {
        gson = new Gson();
        preferences = context.getSharedPreferences(getClass().getPackage().getName(), Context.MODE_PRIVATE);
    }
}
