package cz.olympikstudio.levneknihy.source;

import java.util.List;

import cz.olympikstudio.levneknihy.data.bo.IModelable;

/* Here, we agreed that model is stored in implemented class.
 Though we must provide functions for manipulation with db based on model */
public interface IModel<T>  {
    String getIdent();

    List<? extends IModelable> get(T model);

    void fulltext(T model, boolean cache, String search);

    void insert(T model, List<? extends IModelable> entities);

    void update(T model, List<? extends IModelable> entities);

    void erase(T model, List<? extends IModelable> entities);

    /* Please provide null support. Null = Syn all*/
    void syn(T model);
}