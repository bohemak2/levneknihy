package cz.olympikstudio.levneknihy.source;

import android.content.Context;

import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.olympikstudio.levneknihy.data.Database;
import cz.olympikstudio.levneknihy.data.bo.IModelable;
import cz.olympikstudio.levneknihy.data.bo.book.Book;
import cz.olympikstudio.levneknihy.data.bo.book.BookSaved;
import cz.olympikstudio.levneknihy.data.bo.branch.Branch;
import cz.olympikstudio.levneknihy.source.parser.EParseType;
import cz.olympikstudio.levneknihy.source.parser.IBookSourceParser;
import cz.olympikstudio.levneknihy.source.parser.IBranchSourceParser;
import cz.olympikstudio.levneknihy.source.parser.Parser;

public class LevneKnihy extends Source implements IBookSourceParser, IBranchSourceParser, IModel<LevneKnihyEModel> {
    Database database;

    /* No setting new Objects - it needs to hold reference for BookAdapter in MainActivity */
    /* For filtering, the best way is to have two groups and loop over them. The Android Room doesn't provide enough power for onChange events */
    List<Book> books = new ArrayList<>();
    List<Book> booksHidden = new ArrayList<>();

    List<BookSaved> booksSaved = new ArrayList<>();
    List<BookSaved> booksSavedHidden = new ArrayList<>();

    List<Branch> branches = new ArrayList<>();
    List<Branch> branchesHidden = new ArrayList<>();

    String search;

    public LevneKnihy(Context context, Database database) {
        super(context);

        this.database = database;

        syn(null);
    }

    @Override
    public String getDomain() {
        return "https://www.levneknihy.cz";
    }

    @Override
    public boolean isPagable() {
        return false;
    }

    @Override
    public boolean isNameInUrl() {
        return true;
    }

    @Override
    public boolean isFulltext() {
        return true;
    }

    @Override
    public String getBooksListUrl() {
        return getDomain() + "/sitemap-products.xml";
    }

    @Override
    public String getBranchListUrl() {
        return getDomain() + "/sitemap-content";
    }

    @Override
    public String getBranchId(Document document) {
        // TODO: Real ID
        return document.location();
    }

    @Override
    public String getBranchAddress(Document document) {
        return  document.getElementsByAttributeValue("itemprop", "postalCode").get(0).text() + ", "
                + document.getElementsByAttributeValue("itemprop", "streetAddress").get(0).text() + ", "
                + document.getElementsByAttributeValue("itemprop", "postalCode").get(0).text();
    }

    @Override
    public String getBranchName(Document document) {
        return document.getElementsByTag("h1").get(0).text();
    }

    @Override
    public String getBranchHours(Document document) {
        return document.getElementsByAttributeValue("itemprop", "openingHours").get(0).text();
    }

    @Override
    public float getBranchLng(Document document) {
        Matcher m = Pattern.compile("'lng': (.*)").matcher(document.html());

        // This run matching (without - null)
        if (m.find()){
            return Float.valueOf(m.group(1));
        }

        return 0;
    }

    @Override
    public float getBranchLat(Document document) {
        Matcher m = Pattern.compile("'lat': (.*),").matcher(document.html());

        // This run matching (without - null)
        if (m.find()){
            return Float.valueOf(m.group(1));
        }

        return 0;
    }

    @Override
    public String getBookId(Document document) {
        try {
            return Parser.Utils.getDocumentAttribute(document, "data-product-id");
        } catch (Exception e){
            String[] path = document.location().split("(/)");
            return path[path.length - 1];
        }
    }

    @Override
    public String getBookAuthor(Document document) {
        String result;
        try {
            result = Parser.Utils.getDocumentAttribute(document, "data-product-author");
        } catch (Exception e){
            String h1[] = getH1Split(document);
            result = h1[h1.length - 1];
        }
        return result;
    }

    @Override
    public String getBookName(Document document) {
        String result;
        try {
            result = Parser.Utils.getDocumentAttribute(document, "data-product-name");
        } catch (Exception e){
            result = getH1Split(document)[0];
        }
        return result;
    }

    @Override
    public String getBookDescription(Document document) {
        String result = document.getElementsByAttributeValue("itemprop", "description").get(0).text();
        if (result.length() == 0){
            // Chyba v DOM LK
            result = document.getElementById("description").text().replace("Anotace ", "");
        }
        return result;
    }

    @Override
    public float getBookPrice(Document document) {
        return Float.parseFloat(document.getElementsByClass("product-price").get(0).text().replaceAll("\\D+",""));
    }

    @Override
    public String getImage(Document document) {
        return getDomain() + document.getElementsByAttributeValue("itemprop", "image").get(0).attr("src");
    }

    private String[] getH1Split(Document document){
        return document.getElementsByTag("h1").get(0).text().split("(/)");
    }

    public String getBookAvailabilityUrl(Book book){
        return getDomain() + "/umbraco/Surface/CatalogDetailSurface/GetDeliveryDate?productId={productId}&currencyId=1&selectedBranchOffice=0".replace("{productId}", book.getId());
    }

    // Fetch fresh data
    public void downloadList(LevneKnihyEModel model) {
        // TODO: Start loading, e.g. R.id.content_text
        try {
//            I will just leave those 'nice' functions for future generations. API 16 <<<<<<<< API 24 (.apply)
//            BiFunction<LevneKnihyEModel, String, Boolean> filterUrl =  (m, u) -> {
//                switch (m) {
//                    case BRANCH :
//                        return u.contains("/prodejny/");
//                    default:
//                        return true;
//                }
//            };
//            filterUrl(model, url);
//
//            Function <LevneKnihyEModel, String> url = (m) -> {
//                switch (model){
//                    case BOOK:
//                        return getBooksListUrl();
//                    case BRANCH:
//                        return getBranchListUrl();
//                    default:
//                        return null;
//                }
//            };
//            url.apply(model);


        List<String> urls = new Parser(EParseType.HTML).execute(urlList(model)).get().getElementsByTag("loc").eachText();
        List<String> urlsKnown = new ArrayList<>();

        List<IModelable> entities = new ArrayList<>();

        // Not best, but safe. Therefore, M+N is ok...
        for (IModelable entity : get(model)){
            urlsKnown.add(entity.getUrl());
        }

        for (String urlEntity : urls){
            if (!urlsKnown.contains(urlEntity) && urlFilter(model, urlEntity)) {
                // Potential leak - each IModelable has to have url constructor
                entities.add(model.classBO().getDeclaredConstructor(String.class, String.class).newInstance(urlEntity, getIdent()));
            }
        }

        insert(model, entities);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // TODO: Close loading
        }
    }

    private String urlList(LevneKnihyEModel model ){
        switch (model){
            case BOOK:
                return getBooksListUrl();
            case BRANCH:
                return getBranchListUrl();
            default:
                return null;
        }
    }

    private boolean urlFilter(LevneKnihyEModel model, String url){
        switch (model) {
            case BRANCH :
                return url.contains("/prodejny/");
            default:
                return true;
        }
    }

    public <T extends IModelable> void downloadDetail(LevneKnihyEModel model, T entity) {
        List<IModelable> _t = new ArrayList<>();

            try {
                // TODO: Start loading, e.g. R.id.content_text
                Document document = new Parser(EParseType.HTML).execute(entity.getUrl()).get();

                switch (model){
                    case BOOK:
                    case BOOK_SAVED:
                        Book e = ((Book)entity);

                        e.setId(getBookId(document));
                        e.setName(getBookName(document));
                        e.setAuthor(getBookAuthor(document));
                        e.setDescription(getBookDescription(document));
                        e.setPrice(getBookPrice(document));
                        e.setImageUrl(getImage(document));
                        e.setCheckedAt(new Date());

                        break;
                    case BRANCH:
                        Branch b = (Branch)entity;

                        b.setId(getBranchId(document));
                        b.setName(getBranchName(document));
                        b.setAddress(getBranchAddress(document));
                        b.setHours(getBranchHours(document));
                        b.setLat(getBranchLat(document));
                        b.setLng(getBranchLng(document));
                        b.setCheckedAt(new Date());

                        break;
                }

                _t.add(entity);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // TODO: Close loading
            }

            update(model, _t);
    }

    @Override
    public void insert(LevneKnihyEModel model, List<? extends IModelable> entities) {
        switch (model){
            case BOOK:
                database.getBookDao().insert((List<Book>) entities);
                break;
            case BOOK_SAVED:
                database.getBookSavedDao().insert((List<BookSaved>) entities);
                break;
            case BRANCH:
                database.getBranchDao().insert((List<Branch>) entities);
                break;
        }
        syn(model);
    }


    // No Syn! (automatically through reference)
    @Override
    public void update(LevneKnihyEModel model, List<? extends IModelable> entities) {
        switch (model){
            case BOOK:
                database.getBookDao().update((List<Book>) entities);
                break;
            case BOOK_SAVED:
                database.getBookSavedDao().update((List<BookSaved>) entities);
                break;
            case BRANCH:
                database.getBranchDao().update((List<Branch>) entities);
                break;
        }
    }

    @Override
    public void erase(LevneKnihyEModel model, List<? extends IModelable> entities) {
        switch (model){
            case BOOK:
                database.getBookDao().delete((List<Book>) entities);
                break;
            case BOOK_SAVED:
                database.getBookSavedDao().delete((List<BookSaved>) entities);
                break;
            case BRANCH:
                database.getBranchDao().delete((List<Branch>) entities);
                break;
        }
        syn(model);
    }

    @Override
    public List<? extends IModelable> get(LevneKnihyEModel model) {
        switch (model){
            case BOOK:
                return books;
            case BOOK_SAVED:
                return booksSaved;
            case BRANCH:
                return branches;
        }
        return null;
    }

    public void fulltext(LevneKnihyEModel model, String s){
        fulltext(model, true, s);
    }

    @Override
    public void fulltext(LevneKnihyEModel model, boolean cache, String s) {
        search = s;

        if (cache){
            List<? extends IModelable> entities = null;
            List<? extends IModelable> entitiesHidden = null;

            switch (model){
                case BOOK:
                    entities = books;
                    entitiesHidden = booksHidden;
                    break;

                case BOOK_SAVED:
                    entities = booksSaved;
                    entitiesHidden = booksSavedHidden;
                    break;

                case BRANCH:
                    entities = branches;
                    entitiesHidden = branchesHidden;
                    break;
            }

            ListIterator<? extends IModelable> iter = entities.listIterator();
            while(iter.hasNext()){
                swipeEntity(model, iter.next(), iter,false);
            }

            iter = entitiesHidden.listIterator();
            while(iter.hasNext()){
                swipeEntity(model, iter.next(), iter,true);
            }
        } else {
            if (s.length() > 2){
                String search = "%" + s + "%";

                switch (model){
                    case BOOK:
                        books.clear();
                        books.addAll(database.getBookDao().fulltext(search));
                        break;

                    case BOOK_SAVED:
                        books.clear();
                        books.addAll(database.getBookSavedDao().fulltext(search));
                        break;

                    case BRANCH:
                        branches.clear();
                        branches.addAll(database.getBranchDao().fulltext(search));
                        break;
                }
            }
        }
    }

    private void swipeEntity(LevneKnihyEModel model, IModelable entity, ListIterator<? extends IModelable> iter, boolean filtered){

        switch (model){
            case BOOK:
                Book book = (Book)entity;

                if (book.getUrl().contains(search)){
                    if (filtered){
                        books.add(book);
                        iter.remove();
                    }
                } else if (book.getName() != null && book.getName().contains(search)){
                    if (filtered){
                        books.add(book);
                        iter.remove();
                    }
                } else if (book.getAuthor() != null && book.getAuthor().contains(search)){
                    if (filtered){
                        books.add(book);
                        iter.remove();
                    }
                } else if (book.getDescription() != null && book.getDescription().contains(search)){
                    if (filtered){
                        books.add(book);
                        iter.remove();
                    }
                } else {
                    if (!filtered){
                        iter.remove();
                        booksHidden.add(book);
                    }
                }
                break;

            case BOOK_SAVED:
                BookSaved bookS = (BookSaved) entity;

                if (bookS.getUrl().contains(search)){
                    if (filtered){
                        booksSaved.add(bookS);
                        iter.remove();
                    }
                } else if (bookS.getName() != null && bookS.getName().contains(search)){
                    if (filtered){
                        booksSaved.add(bookS);
                        iter.remove();
                    }
                } else if (bookS.getAuthor() != null && bookS.getAuthor().contains(search)){
                    if (filtered){
                        booksSaved.add(bookS);
                        iter.remove();
                    }
                } else if (bookS.getDescription() != null && bookS.getDescription().contains(search)){
                    if (filtered){
                        booksSaved.add(bookS);
                        iter.remove();
                    }
                } else {
                    if (!filtered){
                        iter.remove();
                        booksSavedHidden.add(bookS);
                    }
                }
                break;

            case BRANCH:
                Branch branch = (Branch)entity;
                if (branch.getUrl().contains(search)){
                    if (filtered){
                        branches.add(branch);
                        iter.remove();
                    }
                } else if (branch.getName() != null && branch.getName().contains(search)){
                    if (filtered){
                        branches.add(branch);
                        iter.remove();
                    }
                } else if (branch.getAddress() != null && branch.getAddress().contains(search)){
                    if (filtered){
                        branches.add(branch);
                        iter.remove();
                    }
                } else {
                    if (!filtered){
                        iter.remove();
                        branchesHidden.add(branch);
                    }
                }
                break;
        }
    }

    // Syn used just for insert + erase, update is on reference
    @Override
    public void syn(LevneKnihyEModel model) {
        if (model == null){
            syn(LevneKnihyEModel.BOOK);
            syn(LevneKnihyEModel.BOOK_SAVED);
            syn(LevneKnihyEModel.BRANCH);
        } else {
            switch (model){
                case BOOK:
                    books.clear();
                    books.addAll(database.getBookDao().getAll());
                    break;
                case BOOK_SAVED:
                    booksSaved.clear();
                    booksSaved.addAll(database.getBookSavedDao().getAll());
                    break;
                case BRANCH:
                    branches.clear();
                    branches.addAll(database.getBranchDao().getAll());
                    break;
            }
        }
    }

    @Override
    public String getIdent() {
        return "lk";
    }
}
