package cz.olympikstudio.levneknihy.source;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import cz.olympikstudio.levneknihy.data.bo.Review;
import cz.olympikstudio.levneknihy.source.parser.IReviewSourceParser;

public class DatabazeKnih extends Source implements IReviewSource, IReviewSourceParser {
    List<String> reviewsUrls = new ArrayList<>();
    List<Review> reviews = new ArrayList<>();

    public DatabazeKnih(Context context) {
        super(context);
//        TODO: On start from SQL
//        reviewsUrls;
//        reviews;
    }


    @Override
    public boolean isFulltext() {
        return false;
    }

    @Override
    public String getReviewsListUrl() {
        return  getDomain() + "/sitemaps/books_{page}.xml";
    }

    @Override
    public String getDomain() {
        return "https://www.databazeknih.cz";
    }

    @Override
    public boolean isPagable() {
        return true;
    }

    @Override
    public boolean isNameInUrl() {
        return true;
    }

    public List<Review> getReviews() {
        return getReviews(true);
    }

    @Override
    public List<Review> getReviews(boolean cache) {
        if (cache){
            return reviews;
        }

        // Fetch fresh data
        reviewsUrls = new ArrayList<>();

//      TODO: as in LK class
//       if (isPagable())
//            for (int i = 0; i < 10 ; i++)
//                reviewsUrls.addAll(Parser.parseHtml(getReviewsListUrl().replace("{page}", Integer.toString(i))).getElementsByTag("loc").eachText());
//        else
//            reviewsUrls.addAll(Parser.parseHtml(getReviewsListUrl()).getElementsByTag("loc").eachText());

// TODO: call on each url and parse review

        return reviews;
    }
}
