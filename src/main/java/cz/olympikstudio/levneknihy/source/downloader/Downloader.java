package cz.olympikstudio.levneknihy.source.downloader;

import android.os.AsyncTask;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

public class Downloader<T> extends AsyncTask<URL, Integer, T> {
    EDownloadType downloadType;

    public Downloader(EDownloadType downloadType) {
        this.downloadType = downloadType;
    }


    @Override
    protected T doInBackground(URL... urls) {
        switch (downloadType){
            case BYTES:
                try {
                    return (T) IOUtils.toByteArray( urls[0].openStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case JSON:
                try {
                    return (T) new JSONObject(IOUtils.toString(urls[0].openStream()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
        return null;
    }
}
