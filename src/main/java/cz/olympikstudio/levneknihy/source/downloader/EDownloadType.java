package cz.olympikstudio.levneknihy.source.downloader;

public enum EDownloadType {
    BYTES, JSON;
}
