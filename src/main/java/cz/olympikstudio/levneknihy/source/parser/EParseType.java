package cz.olympikstudio.levneknihy.source.parser;

public enum EParseType {
    HTML, XML;
}
