package cz.olympikstudio.levneknihy.source.parser;

public interface IReviewSourceParser {
    String getReviewsListUrl();
}
