package cz.olympikstudio.levneknihy.source.parser;

import org.jsoup.nodes.Document;

public interface IBranchSourceParser {
    String getBranchListUrl();

    String getBranchId(Document document);
    String getBranchName(Document document);
    String getBranchAddress(Document document);
    String getBranchHours(Document document);

    float getBranchLat(Document document);
    float getBranchLng(Document document);
}
