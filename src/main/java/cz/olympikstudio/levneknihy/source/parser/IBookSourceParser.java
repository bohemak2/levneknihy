package cz.olympikstudio.levneknihy.source.parser;

import org.jsoup.nodes.Document;

public interface IBookSourceParser {
    String getBooksListUrl();

    String getBookId(Document document);
    String getBookAuthor(Document document);
    String getBookName(Document document);
    String getBookDescription(Document document);
    float getBookPrice(Document document);
    String getImage(Document document);
}
