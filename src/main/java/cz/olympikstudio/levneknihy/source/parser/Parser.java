package cz.olympikstudio.levneknihy.source.parser;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Parser extends AsyncTask<String, Integer, Document> {
    EParseType parseType;

    public Parser(EParseType parseType) {
        super();

        this.parseType = parseType;
    }

    private Document parseHtml(String url) {
        try {
            return Jsoup.connect(url).get();
        } catch( Exception e ){
            return new Document("");
        }
    }

    private Document parseXml(String url) {
        return  Jsoup.parse(parseHtml(url).toString(), "", org.jsoup.parser.Parser.xmlParser());
    }


    @Override
    protected Document doInBackground(String... strings) {
        switch (parseType){
            case HTML:
                return parseHtml(strings[0]);
            case XML:
                return parseXml(strings[0]);
            default:
                return new Document("");
        }
    }


    public static class Utils {

        public static String getDocumentAttribute(Document document, String attribute){
            return document.getElementsByAttribute(attribute).get(0).attr(attribute);
    }

    }
}
