package cz.olympikstudio.levneknihy.source;

public interface ISource {
    String getDomain();

    boolean isPagable();
    boolean isNameInUrl();
    boolean isFulltext();
}
