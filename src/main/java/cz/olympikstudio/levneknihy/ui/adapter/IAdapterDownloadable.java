package cz.olympikstudio.levneknihy.ui.adapter;

public interface IAdapterDownloadable<T> extends IAdapterClickable<T>{
    void onDownloadClick(T item, int i);
}
