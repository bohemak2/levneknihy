package cz.olympikstudio.levneknihy.ui.adapter;

import androidx.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import cz.olympikstudio.levneknihy.data.bo.branch.Branch;

abstract public class BranchAdapter extends ModelableAdapter<Branch> {
    private enum EAdapterItem {
        NAME, ICON
    }

    public BranchAdapter(List<Branch> entities) {
        super(entities);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        final LinearLayout layout = new LinearLayout(viewGroup.getContext());

        int iconSize = 80;
        int textWidth = viewGroup.getWidth() - iconSize;

        int padding = 4;
        /* Text */
        final TextView text = new TextView(layout.getContext());
        text.setPadding(padding, padding, padding, padding);
        text.setWidth(textWidth);

        /* Icon */
        final ImageButton icon = new ImageButton(layout.getContext());
        icon.setMaxWidth(iconSize);
        icon.setMaxHeight(iconSize);


        return new ViewHolder<>(layout,
                new HashMap<EAdapterItem, View>(){
                    {
                        put(EAdapterItem.NAME, text);
                        put(EAdapterItem.ICON, icon);
                    }
                }
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        final Branch branch = entities.get(i);

        final TextView text = (TextView)viewHolder.getItem(EAdapterItem.NAME);
        final ImageButton icon = (ImageButton)viewHolder.getItem(EAdapterItem.ICON);

        if (text != null){
            text.setText(getText(branch));
            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BranchAdapter.this.onItemClick(branch, i);

                    if (!isLoaded(branch)){
                        tryLoad(branch, i, icon);
                    }
                }
            });
        }

        if (icon != null){
            icon.setImageResource(getIconRes(branch));
            icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tryLoad(branch, i, icon);
                }
            });
        }
    }

    private String getText(Branch branch){
        String text = branch.getUrl();

        if (branch.getName() != null && branch.getName().length() > 0){
            text = branch.getName();
        }

        return text;
    }
}
