package cz.olympikstudio.levneknihy.ui.adapter;
import android.widget.ImageView;

import java.util.List;

import cz.olympikstudio.levneknihy.R;
import cz.olympikstudio.levneknihy.data.bo.ILoadable;

abstract public class ModelableAdapter<T extends ILoadable> extends BasicAdapter implements IAdapterDownloadable<T>{
    protected List<T> entities;

    public ModelableAdapter(List<T> entities) {
        this.entities = entities;
    }

    @Override
    public int getItemCount() {
        return entities.size();
    }

    protected void tryLoad(T entity, int index, ImageView icon){
        if (icon != null){
            onDownloadClick(entity, index);
            icon.setImageResource(R.drawable.ic_more_horiz);
        }
    }


    protected boolean isLoaded(T entity){
        return entity.getName() != null && entity.getName().length() > 0;
    }

    protected int getIconRes(T entity){
        if (isLoaded(entity)){
            return R.drawable.ic_refresh;
        } else {
            return R.drawable.ic_file_download;
        }
    }
}
