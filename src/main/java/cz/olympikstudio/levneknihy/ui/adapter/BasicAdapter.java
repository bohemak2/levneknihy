package cz.olympikstudio.levneknihy.ui.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import java.util.Map;

public abstract class BasicAdapter extends RecyclerView.Adapter<BasicAdapter.ViewHolder> {
    public static class ViewHolder<T> extends RecyclerView.ViewHolder{

        // Keep reference for update value
        protected Map<T, View> items;

        public ViewHolder(LinearLayout layout) {
            this(layout, null);
        }

        public ViewHolder(LinearLayout layout, Map<T, View> items) {
            super(layout);

            // Get reference
            this.items = items;
            // And addToView
            for(View item : this.items.values()){
                layout.addView(item);
            }
        }

        public View getItem(T key){
            return items.get(key);
        }
    }
}
