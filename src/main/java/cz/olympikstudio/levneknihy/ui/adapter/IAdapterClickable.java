package cz.olympikstudio.levneknihy.ui.adapter;

interface IAdapterClickable<T> {
    void onItemClick(T item, int i);
}
