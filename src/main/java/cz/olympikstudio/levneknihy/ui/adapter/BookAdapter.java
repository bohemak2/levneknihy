package cz.olympikstudio.levneknihy.ui.adapter;

import androidx.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedHashMap;
import java.util.List;

import cz.olympikstudio.levneknihy.data.bo.book.Book;


abstract public class BookAdapter extends ModelableAdapter<Book> {
    private enum EAdapterItem {
        NAME, AUTHOR, PRICE, ICON
    }

    public BookAdapter(List<Book> entities) {
        super(entities);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        final LinearLayout layout = new LinearLayout(viewGroup.getContext());

        /* WIDTHS */
        int priceWidth = 80;
        int iconWidth = 80;

        int restWidth = viewGroup.getWidth() - priceWidth - iconWidth;
        int authorWidth = (int)(restWidth * 0.2);
        int nameWidth = (int)(restWidth * 0.8);

        int padding = 4;

        /* Name */
        final TextView name = new TextView(layout.getContext());
        name.setPadding(padding, padding, padding, padding);
        name.setWidth(nameWidth);

        /* Author */
        final TextView author = new TextView(layout.getContext());
        author.setWidth(authorWidth);

        /* Price */
        final TextView price = new TextView(layout.getContext());
        price.setGravity(Gravity.CENTER);
        price.setWidth(priceWidth);

        /* Icon */
        final ImageButton icon = new ImageButton(layout.getContext());
        icon.setMaxWidth(iconWidth);


        return new ViewHolder<>(layout,
                new LinkedHashMap<EAdapterItem, View>(){
                {
                    put(EAdapterItem.NAME, name);
                    put(EAdapterItem.AUTHOR, author);
                    put(EAdapterItem.PRICE, price);
                    put(EAdapterItem.ICON, icon);
                }
            }
        );
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final Book book = entities.get(i);

        final TextView name = (TextView)viewHolder.getItem(EAdapterItem.NAME);
        final TextView author = (TextView)viewHolder.getItem(EAdapterItem.AUTHOR);
        final TextView price = (TextView)viewHolder.getItem(EAdapterItem.PRICE);
        final ImageButton icon = (ImageButton)viewHolder.getItem(EAdapterItem.ICON);

        if (name != null){
            name.setText(getName(book));
            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BookAdapter.this.onItemClick(book, i);

                    if (!isLoaded(book)){
                        tryLoad(book, i, icon);
                    }
                }
            });
        }

        if (author != null){
            author.setText(getAuthor(book));
        }

        if (price != null){
            price.setText(getPrice(book));
        }

        if (icon != null){
            icon.setImageResource(getIconRes(book));
            icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tryLoad(book, i, icon);
                }
            });
        }
    }


    private String getAuthor(Book book){
        if (isLoaded(book)) {
            return book.getAuthor();
        }

        return "";
    }

    private String getName(Book book){
        if (isLoaded(book)) {
            return book.getName();
        }

        return book.getUrl();
    }

    private String getPrice(Book book){
        if (isLoaded(book)) {
            return String.valueOf((int)book.getPrice());
        }

        return "";
    }
}
