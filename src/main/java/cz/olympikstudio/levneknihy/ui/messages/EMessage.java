package cz.olympikstudio.levneknihy.ui.messages;

public enum EMessage {
    DIALOG, SNACKBAR;
}
