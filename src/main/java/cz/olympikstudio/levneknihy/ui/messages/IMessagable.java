package cz.olympikstudio.levneknihy.ui.messages;

public interface IMessagable<T> {
    void showMessage(T type, String text);
    void showMessage(T type, int textRes);
}
