package cz.olympikstudio.levneknihy.ui.buttons;

public enum EFloatableButton {
    LEFT_BIG, LEFT_SMALL, RIGHT_BIG, RIGHT_SMALL;
}
