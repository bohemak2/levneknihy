package cz.olympikstudio.levneknihy.ui.buttons;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.view.View;

public interface IFloatable<T> {
    FloatingActionButton getFloatableButton(T type);
    void setFloatableButton(T type, int colorRes, int imageRes, View.OnClickListener listener);
}
