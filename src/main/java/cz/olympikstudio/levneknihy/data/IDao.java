package cz.olympikstudio.levneknihy.data;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Update;

import java.util.List;

public interface IDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(List<T> items);
    @Update
    public void update(List<T> items);
    @Delete
    public void delete(List<T> items);
}
