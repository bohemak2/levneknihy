package cz.olympikstudio.levneknihy.data;

import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;

import cz.olympikstudio.levneknihy.data.bo.book.Book;
import cz.olympikstudio.levneknihy.data.bo.book.BookSaved;
import cz.olympikstudio.levneknihy.data.bo.book.IBook;
import cz.olympikstudio.levneknihy.data.bo.book.IBookSaved;
import cz.olympikstudio.levneknihy.data.bo.branch.Branch;
import cz.olympikstudio.levneknihy.data.bo.branch.IBranch;


@androidx.room.Database(entities = {Book.class, BookSaved.class, Branch.class}, version = 11, exportSchema = false)
@androidx.room.TypeConverters({Convertors.ListConvertor.class, Convertors.DateConverter.class})
public abstract class Database extends RoomDatabase {

    private static Database database;

    public abstract IBook getBookDao();
    public abstract IBookSaved getBookSavedDao();
    public abstract IBranch getBranchDao();

    public static Database getDB(Context content){
        if (database == null){
            database = Room.databaseBuilder(content, Database.class, "lk").allowMainThreadQueries().fallbackToDestructiveMigration().build();
        }
        return database;
    }
}
