package cz.olympikstudio.levneknihy.data.bo;

import java.util.List;

public interface IListable {
    List<?> getAll();
}
