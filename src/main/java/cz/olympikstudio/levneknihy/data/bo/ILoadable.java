package cz.olympikstudio.levneknihy.data.bo;

public interface ILoadable {
    String getName();
}
