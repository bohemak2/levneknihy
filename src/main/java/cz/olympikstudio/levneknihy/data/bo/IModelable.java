package cz.olympikstudio.levneknihy.data.bo;

public interface IModelable {
    String getUrl();
    void setUrl(String url);

    String getIdent();
    void setIdent(String ident);
}
