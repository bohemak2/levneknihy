package cz.olympikstudio.levneknihy.data.bo.book;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.olympikstudio.levneknihy.data.bo.ICheckable;
import cz.olympikstudio.levneknihy.data.bo.IImageable;
import cz.olympikstudio.levneknihy.data.bo.ILoadable;
import cz.olympikstudio.levneknihy.data.bo.IModelable;
import cz.olympikstudio.levneknihy.data.bo.branch.Branch;

@Entity(tableName = "books")
public class Book implements IModelable, ILoadable, ICheckable, IImageable {

    @PrimaryKey
    @NonNull
    private String url;

    private String id;
    private String name;
    private String author;
    private String description;
    private float price;
    private Date checkedAt;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] image;
    private String imageUrl;

    private String ident;

    private List<String> branches;

    public Book(String url, String ident) {
        this.url = url;
        this.ident = ident;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCheckedAt() {
        return checkedAt;
    }

    public void setCheckedAt(Date checkedAt) {
        this.checkedAt = checkedAt;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getIdent() {
        return ident;
    }
    public void setIdent(String ident) {
        this.ident = ident;
    }


    public List<String> getBranches() {
        return branches;
    }

    public void setBranches(List<String> branches) {
        this.branches = branches;
    }

    public List<Branch> getBranchesBO(List<Branch> branches) {
        List<Branch> result = new ArrayList<>();

        Map<String, Branch> hashedBranches= new HashMap<>();
        for (Branch branch : branches){
            hashedBranches.put(branch.getId(), branch);
        }

        for(String branchId : this.branches){
            result.add(hashedBranches.get(branchId));
        }

        return result;
    }

    public void setBranchesBO(List<Branch> branches) {
        this.branches = new ArrayList<>();
        for(Branch branch : branches){
            this.branches.add(branch.getId());
        }
    }
}
