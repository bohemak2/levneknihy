package cz.olympikstudio.levneknihy.data.bo;
import java.util.Date;
public interface ICheckable {
    Date getCheckedAt();
}
