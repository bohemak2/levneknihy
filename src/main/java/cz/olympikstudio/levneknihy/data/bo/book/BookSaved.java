package cz.olympikstudio.levneknihy.data.bo.book;

import androidx.room.Entity;

/* NOTE: Proc vlastne je to trida a ne atribut? Protoze
    a) Chceme uchovat "casovou" kopii (vcetne dostupnych pobocek)
    b) mame moznost smazat veskere knihy, smazali bychom i tu s atributem 'saved' */

@Entity(tableName = "books_saved")
public class BookSaved extends Book {

    public BookSaved(String url, String ident) {
        super(url, ident);
    }
}
