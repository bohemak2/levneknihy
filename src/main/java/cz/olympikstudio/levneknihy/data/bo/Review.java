package cz.olympikstudio.levneknihy.data.bo;

import cz.olympikstudio.levneknihy.data.bo.book.Book;
import cz.olympikstudio.levneknihy.source.parser.IReviewSourceParser;

public class Review {
    private Book book;
    private IReviewSourceParser provider;
    private float mark;

    public Review(Book book, IReviewSourceParser provider, float mark) {
        this.book = book;
        this.provider = provider;
        this.mark = mark;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public IReviewSourceParser getProvider() {
        return provider;
    }

    public void setProvider(IReviewSourceParser provider) {
        this.provider = provider;
    }

    public float getMark() {
        return mark;
    }

    public void setMark(float mark) {
        this.mark = mark;
    }
}
