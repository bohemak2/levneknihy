package cz.olympikstudio.levneknihy.data.bo.book;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import cz.olympikstudio.levneknihy.data.IDao;
import cz.olympikstudio.levneknihy.data.bo.IFulltext;
import cz.olympikstudio.levneknihy.data.bo.IListable;

@Dao
public interface IBookSaved extends IFulltext, IListable, IDao<BookSaved> {
    @Query("SELECT * FROM books_saved WHERE (name LIKE :search OR author LIKE :search OR url LIKE :search OR description LIKE :search)")
    List<BookSaved> fulltext(String search);

    @Query("SELECT * FROM books_saved ORDER by checkedAt DESC")
    List<BookSaved> getAll();

    @Query("SELECT * FROM books_saved WHERE id=:id")
    BookSaved getOne(String id);
}