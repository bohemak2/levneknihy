package cz.olympikstudio.levneknihy.data.bo.branch;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import cz.olympikstudio.levneknihy.data.IDao;
import cz.olympikstudio.levneknihy.data.bo.IFulltext;
import cz.olympikstudio.levneknihy.data.bo.IListable;

@Dao
public interface IBranch extends IFulltext, IListable, IDao<Branch> {
    @Query("SELECT * FROM branches WHERE name LIKE :search OR url LIKE :search")
    List<Branch> fulltext(String search);

    @Query("SELECT * FROM branches ORDER BY checkedAt DESC")
    List<Branch> getAll();

    @Query("SELECT * FROM branches WHERE id=:id")
    Branch getOne(String id);
}