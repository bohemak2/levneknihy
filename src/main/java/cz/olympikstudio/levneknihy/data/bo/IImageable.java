package cz.olympikstudio.levneknihy.data.bo;

public interface IImageable {
    byte[] getImage();
}
