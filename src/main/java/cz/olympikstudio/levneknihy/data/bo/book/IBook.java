package cz.olympikstudio.levneknihy.data.bo.book;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import cz.olympikstudio.levneknihy.data.IDao;
import cz.olympikstudio.levneknihy.data.bo.IFulltext;
import cz.olympikstudio.levneknihy.data.bo.IListable;

@Dao
public interface IBook extends IFulltext, IListable, IDao<Book> {
    @Query("SELECT * FROM books WHERE (name LIKE :search OR author LIKE :search OR url LIKE :search OR description LIKE :search)")
    List<Book> fulltext(String search);

    @Query("SELECT * FROM books ORDER by checkedAt DESC")
    List<Book> getAll();

    @Query("SELECT * FROM books WHERE id=:id")
    Book getOne(String id);
}