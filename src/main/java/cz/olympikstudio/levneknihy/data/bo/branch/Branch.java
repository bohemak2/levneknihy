package cz.olympikstudio.levneknihy.data.bo.branch;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import java.util.Date;

import cz.olympikstudio.levneknihy.data.bo.ICheckable;
import cz.olympikstudio.levneknihy.data.bo.ILoadable;
import cz.olympikstudio.levneknihy.data.bo.IModelable;

@Entity(tableName = "branches")
public class Branch implements IModelable, ILoadable, ICheckable {
    private String name;
    private String address;
    private String hours;

    private float lat;
    private float lng;

    private Date checkedAt;

    private String ident;

    public Branch(String url, String ident){
        this.url = url;
        this.ident = ident;
    }

    @PrimaryKey
    @NonNull
    private String url;

    private String id;

    @NonNull
    public String getUrl() {
        return url;
    }

    public void setUrl(@NonNull String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    @Override
    public Date getCheckedAt() {
        return checkedAt;
    }

    public void setCheckedAt(Date checkedAt) {
        this.checkedAt = checkedAt;
    }
}
