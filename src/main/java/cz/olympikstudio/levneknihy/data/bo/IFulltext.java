package cz.olympikstudio.levneknihy.data.bo;

import java.util.List;

public interface IFulltext {
    List<?> fulltext(String s);
}
