package cz.olympikstudio.levneknihy.data;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Date;
import java.util.List;

public class Convertors {

    public static class ListConvertor {
        @TypeConverter
        public static List<String> fromString(String value) {
            return new Gson().fromJson(value, new TypeToken<List<String>>() {
            }.getType());
        }

        @TypeConverter
        public static String toString(List<String> list) {
            return new Gson().toJson(list);
        }
    }

    public static class DateConverter {

        @TypeConverter
        public Date toDate(Long dateLong){
            return dateLong == null ? null: new Date(dateLong);
        }

        @TypeConverter
        public Long fromDate(Date date){
            return date == null ? null : date.getTime();
        }
    }
}
